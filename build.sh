#!/bin/sh

# Title: aplayer builder
# Author: Lukas Kasticky <lukas@kasticky.com>
# Dependencies:
#       sass (https://sass-lang.com/),
#       terser (https://github.com/terser-js/terser),
#       inotify-tools

# files (relative to "src/"")
files_sass=("aplayer-dark.scss")
files_js=("aplayer.js")

# Get options
opt_h=false
opt_b=false
opt_l=false
sass="sass"
terser="terser"
while getopts 'hbls:t:' opt; do
    case $opt in
        h) opt_h=true;;
        b) opt_b=true;;
        l) opt_l=true;;
        s) sass="$OPTARG";;
        u) terser="$OPTARG";;
    esac
done

# Show help
function help {
    echo "Usage: build.sh [options]

Description:
    Tool for building aplayer

Options:
    -h      show help text
    
    -b      build aplayer
    -l      listen for file changes
    
    -s      specify custom sass command
    -t      specify custom terser command"
    exit
}
if $opt_h; then
    help
fi

# Build
if $opt_b; then
    while true; do
        mkdir -p dist demo
        rm -rdf dist/* demo/*

        # build
        cp "src/demo.html" "demo/index.html"
        for file in ${files_sass[*]}; do
            eval "${sass} src/${file} dist/$(basename ${file} .scss).css --style compressed"
            cp "dist/$(basename ${file} .scss).css" "demo/$(basename ${file} .scss).css"
        done
        for file in ${files_js[*]}; do
            eval "${terser} src/${file} -o dist/$(basename ${file} .js).min.js --compress --mangle"
            cp "dist/$(basename ${file} .js).min.js" "demo/$(basename ${file} .js).min.js"
        done

        # watch changes
        if $opt_l; then
            inotifywait -e modify src/
        else
            exit
        fi
    done
fi

help