(function() {
    class APlayer {
        constructor(title, artist, album, src, artwork = "", credits = {}) {
            this.title = title;
            this.artist = artist;
            this.album = album;
            this.audio = new Audio(src);
            this.artwork = artwork;
            this.credits = credits;
            this.playing = false;
        }
        
        // Generates and returns an HTML element
        generate() {
            let credits = this.credits;
            this.wrapper = document.createElement("div");
            this.wrapper.innerHTML =
`<img class="artwork" src="${this.artwork}" alt="${this.album}">
<div class="controls">
    <button class="aplayer-play"><svg class="pause" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14c-.55 0-1-.45-1-1V9c0-.55.45-1 1-1s1 .45 1 1v6c0 .55-.45 1-1 1zm4 0c-.55 0-1-.45-1-1V9c0-.55.45-1 1-1s1 .45 1 1v6c0 .55-.45 1-1 1z"/></svg><svg class="play" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 13.5v-7c0-.41.47-.65.8-.4l4.67 3.5c.27.2.27.6 0 .8l-4.67 3.5c-.33.25-.8.01-.8-.4z"/></svg></button>
    <div class="text">
        <span class="title">${this.title}</span>
        <span class="artist">${this.artist}</span>
    </div>
</div>
<table class="credits">
    ${ (function() {
        let html = "";
        for (const role in credits) {
            html += "<tr><td>" + role + "</td><td>";
            for (const person of credits[role]) {
                html += person + '\n';
            }
            html += "</td></tr>"
        }
        return html;
    })() }
</table>`;
            this.wrapper.className = "aplayer-wrapper";
            let btn = this.wrapper.getElementsByClassName("aplayer-play")[0];
            btn.addEventListener("click", this.playToggle.bind(this)); // in order to identify elements and their related objects `bind()` is used; e.g.: needed for `playToggle(e)`
            return this.wrapper;
        }

        // Play/Pause audio
        playToggle(e) {
            if (this.playing) {
                this.audio.pause();
                this.wrapper.classList.remove("playing");
            } else {
                this.audio.play();
                this.wrapper.classList.add("playing");
            }
            this.playing = !this.playing;
        }
    }

    window.APlayer = APlayer
})()