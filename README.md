# aplayer

This simplistic music player has been created for use on a promotional page.

![Screenshot](assets/screenshot.png)

## Features

- Play/Pause audio
- Dark theme
- Responsive design

... and that's it at the moment!

## Usage

```html
<link rel="stylesheet" href="aplayer-dark.css">
<script src="aplayer.min.js"></script>
```

```javascript
APlayer: new (title: string, artist: string, album: string, src: string, artwork?: string, credits: Object: Array)
```

### Example

```javascript
let myAudio = new APlayer(
    "Title",
    "Artist",
    "Album",
    "Source (URI)",
    "Artist (URI)",
    {
        "Composer": ["John Doe"],
        "Lyricist": ["Jane Doe"],
        "Main Artists": ["John Doe", "Jane Doe"],
        "Foo": ["Bar"]
    });
document.body.appendChild(myAudio.generate());
```

Have a look at [`demo/`](demo/) for more information

## Contribution

If you feel the project is missing something, feel free to contribute. There are no special guidelines, just common sense :)

### Build

A build script is provided to generate [`dist/`](dist/) and [`demo/`](demo/).

```sh
$ ./build.sh
```

```text
Usage: build.sh [options]

Description:
    Tool for building aplayer

Options:
    -h      show help text
    
    -b      build aplayer
    -l      listen for file changes
    
    -s      specify custom sass command
    -t      specify custom terser comman
```

### Live Preview

If you have PHP installed you can run the following command:

```sh
$ php -S 127.0.0.1:8080 -t demo/
```